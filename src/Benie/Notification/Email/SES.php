<?php
namespace Benie\Notification\Email;
use Aws\Ses\SesClient;

trait SES {
	/**
	 * Sends an email using the sendEmail() method of the SES class.
	 * Future versions of this may use sendRawEmail() for better handling
	 * of custom headers, attachments, etc.
	 */
	public function dispatch() {
		$client = SesClient::factory(array(
			'key' => \Benie\Notification::getVar('BenieEmailUser', 'S3AccessKey', null),
			'secret' => \Benie\Notification::getVar('BenieEmailPass', 'S3SecretKey', null),
			'region' => 'us-east-1',		// No ap-southeast-2 for SES
		));

		$email = array(
			'Source' => $this->sender,
			'Destination' => array(
				'ToAddresses' => $this->getRecipients(),
				// 'CcAddresses' => [],
				// 'BccAddresses' => [],
			),
			'Message' => array(
				'Subject' => array(
					'Data' => $this->subject,
					'Charset' => 'UTF-8',
				),

				'Body' => array(
					'Text' => array(
						'Data' => $this->plain_text,
						'Charset' => 'UTF-8',
					),

					'Html' => array(
						'Data' => $this->body,
						'Charset' => 'UTF-8',
					),
				),
			),
			'ReplyToAddresses' => array($this->sender),
			'ReturnPath' => $this->sender
		);

		$result = $client->sendEmail($email);

		return $result;
	}
}