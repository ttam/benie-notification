<?php
namespace Benie\Notification;

class SMS extends BaseNotification {
	// use \Benie\Notification\SMS\AussieSMS;
	use \Benie\Notification\SMS\SMSBroadcast;

	/**
	 * Make sure the recipient is a valid address, or array of addresses.
	 */
	public function setRecipients($recipients) {
		if(empty($recipients)) return false;
		if(!is_array($recipients)) $recipients = explode(',', $recipients);

		foreach($recipients as $recipient) {
			$recipient = preg_replace('/[^\d]+/', '', trim($recipient));
			if(!empty($recipient)) $this->addRecipient($recipient);
		}

		return false;
	}

	/**
	 * [getRecipients description]
	 * @return [type] [description]
	 */
	public function getRecipients() {
		return implode(',', $this->recipients);
	}

}