<?php
namespace Benie\Notification\SMS;

trait AussieSMS {
	public function dispatch() {

		/**
		 * Build the query string with all required parameters
		 * @var [type]
		 */
		$url = 'http://api.aussiesms.com.au/?sendsms&';
    $url .= http_build_query(array(
			'mobileID' => rawurlencode(getenv('BenieSMSUser')),
			'password' => rawurlencode(getenv('BenieSMSPass')),
			'to' => $this->getRecipients(),
			'text' => $this->body,
			'from' => $this->sender,
			'msg_type' => 'SMS_TEXT',
    ));


    /**
     * Send a cURL request to the server with the data from above
     * @var [type]
     */
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close ($ch);

		/**
		 * Send the raw response string back
		 */
		return $response;
	}
}