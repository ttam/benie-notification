<?php
namespace Benie\Notification\SMS;

trait SMSBroadcast {

	public function dispatch() {
		/**
		 * Build the query string with all required parameters
		 * @var [type]
		 */
    $data = http_build_query(array(
			'username' => rawurlencode(\Benie\Notification::getVar('BenieSMSUser', 'SMSUser', null)),
			'password' => rawurlencode(\Benie\Notification::getVar('BenieSMSPass', 'SMSPass', null)),
			'to' => $this->getRecipients(),
			'from' => rawurlencode($this->sender),
			'message' => $this->body,
			'ref' => rawurlencode(uniqid()),
    ));

    /**
     * Send a cURL request to the server with the data from above
     * @var [type]
     */
		$ch = curl_init('http://api.smsbroadcast.com.au/api-adv.php');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close ($ch);

		/**
		 * Send the raw response string back
		 */
		return $response;
	}
}